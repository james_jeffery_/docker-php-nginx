This is a simple boilerplate to create a development environment with Docker for PHP development.

There's 3 containers:

* Frontend - This is for static frontend content. I generally use Hugo
* API - This is an nginx container for the API seperate from the frontend. It will serve PHP.
* PHP - This is the php-fpm container